numbers = ["9","4","1","3","6","2"]
swap = true
size = numbers.length - 1
while swap
    swap = false
    for i in 0...size
        swap |= numbers[i] > numbers[i + 1] 
        numbers[i], numbers[i + 1] = numbers[i + 1], numbers[i] if numbers[i] > numbers [i + 1]
    end
    size -= 1
end
puts numbers.join(', ')
